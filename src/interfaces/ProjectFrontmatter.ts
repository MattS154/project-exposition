export interface ProjectFrontmatter {
    title: string|undefined
    description: string|undefined
    url: string|undefined
    repositoryUrl: string|undefined
    startDate: string|undefined
    endDate: string|undefined
}