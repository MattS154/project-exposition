---
title: Kittbot
description: The first iteration of a discord bot for some chat servers I'm in. It was very prone to crashing which took down the whole bot.
url: 
repositoryUrl: https://gitlab.com/MattS154/kittbot
startDate: Jul 19, 2021
endDate: Feb 05, 2022
featured: false
---