---
title: Cherry MX Switch 3D Scroll
description: An idea where a switch disassembles its self as the page scrolls down. Uses Vue, VueGL, and AnimeJS to produce the 3D scrolling animation.
url: https://cherry-mx-switch-3d-scroll.mattsmodules.com/
repositoryUrl: https://gitlab.com/MattS154/cherry-mx-switch-3d-scroll
startDate: June 05, 2021
endDate: April 23, 2023
featured: false
---