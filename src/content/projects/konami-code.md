---
title: Konami Code
description: Generates a promise that resolves when the konami code is entered on a webpage.
url: 
repositoryUrl: https://github.com/Matts154/konami-code
startDate: Aug 12, 2018
endDate: Aug 31, 2018
featured: false
---