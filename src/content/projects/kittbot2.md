---
title: Kittbot v2
description: A discord bot that creates containerized microservices to separate out logical components. They all communicate through their own topic on Kafka, a popular message bus. This was a huge improvement over the last iteration with improved stability, separation of concerns, and better developer experience by using Nx.
url: 
repositoryUrl: https://gitlab.com/MattS154/kittbot2
startDate: Mar 30, 2022
endDate: Jul 15, 2023
featured: true
---