---
title: TekI2CDecode
description: This bit of code will set up a Tektronix MSO5204B to read from CH1 and CH2 and decode an I2C signal. It will also save the event log to the Desktop as BusDecode.csv
url: 
repositoryUrl: https://github.com/Matts154/TekI2CDecode
startDate: August 30, 2015
endDate: August 30, 2015
featured: false
---