---
title: Public API Mashup
description: Utilizes davemachado's public API of public APIs to generate a mashup of public APIs for potential project ideas.
url: https://public-api-mashup.mattsmodules.com/
repositoryUrl: https://gitlab.com/MattS154/public-api-mashup
startDate: May 04, 2020
endDate: May 17, 2020
featured: false
---