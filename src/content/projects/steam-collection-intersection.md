---
title: Steam Collection Intersection
description: This will determine which games you have in common with your selected friends and return a list of games. Click on one of the games to launch it in Steam.
url: 
repositoryUrl: https://github.com/Matts154/steam-collection-intersection
startDate: June 3, 2017
endDate: Sep 22, 2017
featured: false
---