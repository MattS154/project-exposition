---
title: Diceware
description: Diceware is a method for creating passphrases, passwords, and other cryptographic variables using ordinary dice as a hardware random number generator. Except this doesn't use dice.
url: https://diceware.mattsmodules.com/
repositoryUrl: https://gitlab.com/MattS154/diceware
startDate: December 24, 2017
endDate: May 28, 2018
featured: true
---