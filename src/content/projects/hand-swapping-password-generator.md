---
title: Hand Swapping Password Generator
description: A really small bit of code that produces a random password while keeping it easy to type. It prompts the user for a password length and generates a password by changing which side of the keyboard a character is chosen from. The idea behind this is to have a password that is immune to dictionary attacks but is easy to type.
url: 
repositoryUrl: https://github.com/Matts154/HSPG
startDate: October 23, 2014
endDate: October 23, 2014
featured: false
---