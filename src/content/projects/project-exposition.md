---
title: Project Exposition
description: You're looking at it! I started this project to show off my other projects and describe my work history.
url: https://www.mattsmodules.com/
repositoryUrl: https://gitlab.com/MattS154/project-exposition
startDate: April 2023
endDate: Present
featured: true
---