---
# Organization details
name: Silverback Migration Solutions
shortName: Silverback
abbreviation: N/A
logo: Silverback-Logo-without-text.png
tagline: Silverback Data Center Solutions, Inc. is an IT services company with a focus on migration, installation, physical support, decommissioning and audit of Enterprise and Commercial Data Center and technical environments. My focus was preparing servers for transport, moving them to their new physical location, and installing the servers in the server rack.

# My involvement
role: Data Center Technician
startDate: July 2011
endDate: July 2012
---