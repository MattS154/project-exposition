---
# Organization details
name: California State University, East Bay
shortName: Cal State East Bay
abbreviation: CSUEB
logo: csueb_social_media.svg
tagline: California State University, East Bay (Cal State East Bay, CSU East Bay, or CSUEB) is a public university in Hayward, California. CSUEB is known for award-winning programs, expert instruction, small classes, and highly-personalized learning environment. I graduated from here with a Bachelor's degree in Computer Science.

# My involvement
role: Student
startDate: September 2012
endDate: June 2015
---