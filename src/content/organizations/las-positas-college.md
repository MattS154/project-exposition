---
# Organization details
name: Las Positas College
shortName: Las Positas College
abbreviation: LPC
logo: lpc-logo-vertical-rgb.png
tagline: Las Positas College (LPC) is a public community college in Livermore, California. It is an inclusive, learning-centered, equity-focused environment that offers educational opportunities and support for completion of students' transfer, degree, and career-technical goals while promoting lifelong learning. I graduated in 2010 with an Associate's degree in Networking Security and Administration.

# My involvement
role: Student
startDate: August 2007
endDate: August 2010
---