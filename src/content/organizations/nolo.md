---
# Organization details
name: Nolo
shortName: Nolo
abbreviation: N/A
logo: Nolo_logo_vertical.svg
tagline: Nolo makes the law accessible to everyone by offering an extensive library of free, legal articles and a catalog of DIY products and legal services designed to help you solve your everyday legal problems. I started my career here testing their DIY products, the store, and their legal lead generation products as a Quality Assurance (QA) intern. I grew into the role and became the main QA engineer for Quicken Willmaker. From there, transitioned out of QA and into a frontend development role where I added features and fixed bugs to all of the products that I had been testing before in QA.

# My involvement
role: QA Intern, Associate QA Engineer, Associate Frontend Engineer
startDate: October 2015
endDate: April 2020
---