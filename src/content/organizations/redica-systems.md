---
# Organization details
name: Redica Systems
shortName: Redica
abbreviation: N/A
logo: Redica_vertical.png
tagline: Redica Systems is a data analytics platform to help regulated industries improve their quality and stay on top of evolving regulations. Here I was part of the team leading up to the initial launch of the Redica Systems data visualization platform. I designed a system to ingest and perform entity resolution for FDA data to use in the platform. I would go on to help evolve the platform by creating enterprise grade software for more data visualization, ticketing, and other features.

# My involvement
role: Software Engineer II
startDate: April 2020
endDate: October 2023
---